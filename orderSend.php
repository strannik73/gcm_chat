<!DOCTYPE HTML>  
<html>
<head>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
<style>
.error {color: #FF0000;}
</style>
</head>
<body>  

<?php
// define variables and set to empty values
$useridErr = $tickerErr = $priceErr = $directionErr = $PriceChangeErr = $ProviderErr = $StatusErr = "";
$userid = $ticker = $price = $direction = $PriceChange = $Provider = $Status = "";
$result = "";

if ($_SERVER["REQUEST_METHOD"] == "POST") {
  if (empty($_POST["UserId"])) {
    $useridErr = "Userid is required";
  } else {
    $url = 'http://localhost:5000/api/orders';
    $fields = array(
      'UserId' => $_POST['UserId'],
      'Ticker' => $_POST['Ticker'],
      'Price' => $_POST['Price'],
      'Direction' => $_POST['Direction'],
      'PriceChange' => $_POST['PriceChange'],
      'Provider' => $_POST['Provider'],
      'Status' => $_POST['Status']
    );
    
    //open connection
    $ch = curl_init($url);
    
    //set the url, number of POST vars, POST data
    $data_string = json_encode($fields);


    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
      'Content-Type: application/json',
      'Content-Length: ' . strlen($data_string)
    ));                
    
    //execute post
    $result = curl_exec($ch);
    
    //close connection
    curl_close($ch);

  }

}

?>
<?php if ($result != null): ?>
<p><?php echo $result ?></p>
<?php endif; ?>
<div class="container">
    <div class="row">
      <div class="col">
        <h2>Add new Order</h2>
      </div>
    </div>
  <div class="row ">
    <div class="col">
      <p><span class="error">* required field.</span></p>
    </div>
  </div>
  
    
      <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" >  
      <div class="form-group">
      <input type="text" name="UserId" value="<?php echo $userid; ?>" class="form-control" placeholder="User id">
        <span class="error">* <?php echo $useridErr; ?></span>
      </div>
    <div class="form-group">
    
    <input type="text" name="Ticker" value="<?php echo $ticker; ?>" class="form-control" placeholder="Ticker">
        <span class="error">* <?php echo $tickerErr; ?></span>
        </div>
 
  
    <div class="form-group">
    <input type="text" name="Price" value="<?php echo $price; ?>" class="form-control" placeholder="Price">
        <span class="error">*<?php echo $priceErr; ?></span>
        </div>
  
  
    <div class="form-group">
    
               <select name="Direction" class="form-control" >  
                    <option value="0">Up</option>
                    <option value="1">Down</option>
                 </select>
    </div>
  
 
    <div class="form-group">
        
        <input type="text" name="PriceChange"  value="<?php echo $PriceChange; ?>" class="form-control" placeholder="PriceChange">
            <span class="error">* <?php echo $PriceChangeErr; ?></span>
    </div>
  
  
    <div class="form-group">
    
          <select name="Provider" class="form-control">  
                          <option value="Bittrex">Bittrex</option>
                          <option value="Bitfinex">Bitfinex</option>
          </select>
    
        <span class="error">*<?php echo $ProviderErr; ?></span>
    </div>
  
    <div class="form-group">
         <input type="text" name="Status" value="0" hidden="true">
    </div>   
        <br><br>
        <input type="submit"  value="Submit">  
      </form>
      
  
</div>

</body>
</html>