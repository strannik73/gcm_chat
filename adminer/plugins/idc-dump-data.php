<?php

class AdminerIdcDumpData
{
    /** @access protected */
    var $filename, $data;

    const OUTPUT = 'idc_sep';

    function dumpOutput() {
        return array(self::OUTPUT => 'only sql data in separate files');
    }

    // this is the first function that is called in dump process
    // here we can unset some settings
    function dumpHeaders() {
        if ($_POST['output'] == self::OUTPUT) {
            $_POST['output'] = 'text';
            $_POST['format'] = self::OUTPUT;
            unset($_POST['db_style']);
            unset($_POST['routines']);
            unset($_POST['events']);
            unset($_POST['table_style']);
            unset($_POST['auto_increment']);
            unset($_POST['triggers']);
            return 'text/plain';
        }
    }

    // copied from adminer.inc.php -> dumpData function
    function dumpData($table, $style, $query) {
        if ($_POST["format"] != self::OUTPUT) {
            return null;
        }
        $idc_result = '';
        $idc_result .= "SET NAMES utf8;\n";

        $connection = connection();
        $max_packet = 1048576;
        if ($style) {
            if ($style == "TRUNCATE+INSERT") {
                $idc_result .= truncate_sql($table) . ";\n";
            }
            $fields = fields($table);
            $result = $connection->query($query, 1); // 1 - MYSQLI_USE_RESULT //! enum and set as numbers
            if ($result) {
                $insert = "";
                $buffer = "";
                $keys = array();
                $suffix = "";
                $fetch_function = ($table != '' ? 'fetch_assoc' : 'fetch_row');
                while ($row = $result->$fetch_function()) {
                    if (!$keys) {
                        $values = array();
                        foreach ($row as $val) {
                            $field = $result->fetch_field();
                            $keys[] = $field->name;
                            $key = idf_escape($field->name);
                            $values[] = "$key = VALUES($key)";
                        }
                        $suffix = ($style == "INSERT+UPDATE" ? "\nON DUPLICATE KEY UPDATE " . implode(", ", $values) : "") . ";\n";
                    }
                    if (!$insert) {
                        $insert = "INSERT INTO " . table($table) . " (" . implode(", ", array_map('idf_escape', $keys)) . ") VALUES";
                    }
                    foreach ($row as $key => $val) {
                        $field = $fields[$key];
                        $row[$key] = ($val !== null
                            ? unconvert_field($field, preg_match('~(^|[^o])int|float|double|decimal~', $field["type"]) && $val != '' ? $val : q($val))
                            : "NULL"
                        );
                    }
                    $s = ($max_packet ? "\n" : " ") . "(" . implode(",\t", $row) . ")";
                    if (!$buffer) {
                        $buffer = $insert . $s;
                    } elseif (strlen($buffer) + 4 + strlen($s) + strlen($suffix) < $max_packet) { // 4 - length specification
                        $buffer .= ",$s";
                    } else {
                        $idc_result .= $buffer . $suffix;
                        $buffer = $insert . $s;
                    }
                }
                if ($buffer) {
                    $idc_result .= $buffer . $suffix;
                }
            } else {
                $idc_result .= "-- " . str_replace("\n", " ", $connection->error) . "\n";
            }
        }

        $filename = realpath(__DIR__ . '/../../data');
        $filename .= '/' . $table . '.sql';
        file_put_contents($filename, $idc_result);
        @chmod($filename, 0777);
        echo "$filename<br>\n";

        return true;
    }
}
